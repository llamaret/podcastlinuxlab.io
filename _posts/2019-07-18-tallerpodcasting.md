---
title: "Taller Podcasting Libre 2019"
date: 2019-07-18
author: juan
category: [taller, podcasting]
featimg: 2019/tallerpodcasting.png
podcast:
  audio: 
  video:
tags: [taller, podcasting]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2019/tallerpodcasting.png)  
El 18 de julio realizo un Taller de Podcasting libre en [TLP Tenerife](https://tlp-tenerife.com/). Aquí te dejo toda la documentación de éste.

**Enlaces de Interés:**
+ [Curso Podcasting Podcast Linux](https://podcastlinux.com/cursopodcasting/)
+ [Curso Audacity 9 Decibelios](https://9decibelios.es/cursos/curso-basico-de-audacity/)
+ [TheAudacityToPodcast.com](https://theaudacitytopodcast.com/chriss-dynamic-compressor-plugin-for-audacity/)
+ [Wiki Audacity](https://wiki.audacityteam.org/wiki/Audacity_Wiki_Home_Page)
+ [Repositorio Gitlab](https://gitlab.com/podcastlinux/tallerpodcasting/)

**Presentación:**  
+ [Archivo .odp](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/TallerPodcastingLibre19.odp)
+ [Archivo pdf](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/charla/TallerPodcastingLibre19.pdf)

**Audacity**
+ [Preset Ecualización Voz](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/audacity/EQVoz.xml)
+ [Filtro Compresión](https://gitlab.com/podcastlinux/tallerpodcasting/raw/master/audacity/compress.ny)


Recuerda que puedes **contactar** conmigo de las siguientes maneras:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
