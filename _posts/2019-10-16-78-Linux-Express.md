---
title: "#78 Linux Express"
date: 2019-10-16
author: juan
category: [linuxexpress]
featimg: 2019/linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/78linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2019/linuxexpress.png)  
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/78linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/78linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#88 Creative Commons](https://avpodcast.net/podcastlinux/creativecommons)
+ Próximo episodio: Linux Connexion con [Irene Soria](https://twitter.com/arenitasoria)
+ Mis primeros proyectos con [Arduino](https://www.arduino.cc/)
+ Calendario, notas, citas y eventos en tu terminal con [Calcurse](https://www.calcurse.org/)
+ Utilizando [Klavaro](https://klavaro.sourceforge.io/en) en mi colegio para trabajar mecanografía. 
+ Próximos Eventos: [Linux y Tapas](https://linuxytapas.wordpress.com/) y [Maratón Pod](https://www.maratonpod.es/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  	
