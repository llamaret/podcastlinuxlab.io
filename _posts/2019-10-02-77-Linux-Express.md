---
title: "#77 Linux Express"
date: 2019-10-02
author: juan
category: [linuxexpress]
featimg: 2019/linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/77linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2019/linuxexpress.png)  
<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/77linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/77linuxexpress.mp3" type="audio/mpeg">
</audio>

Lista de los temas para este episodio:  
+ Episodio [#87 Linux Connexion con Adrián Perales](https://avpodcast.net/podcastlinux/adrianperales)
+ Próximo episodio: [Creative Commons](https://creativecommons.org/)
+ Curso [Maker School](https://t.me/kreitek_anuncios/201) de [Kreitek](https://kreitek.org/) con [Arduino](https://www.arduino.cc/), [Raspberry Pi](https://www.raspberrypi.org/), impresión 3D con [FreeCAD](https://freecadweb.org/) y corte láser con [InkScape](https://inkscape.org/es/).
+ [Fritzing](https://fritzing.org) para crear  esquemas eléctricos en proyectos Arduino.
+ Nueva versión de [ShotCut](https://www.shotcut.org/blog/new-release-190914/)
+ Eventos podcasteros: [Podcast Days](https://www.podcastdays.es/) y [Maratón Pod](https://www.maratonpod.es/)


Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Mastodon: <https://mastodon.social/@podcastlinux/>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.com/>  
Telegram: <https://t.me/podcastlinux>  
Telegram Juan Febles: <https://t.me/juanfebles>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  	
