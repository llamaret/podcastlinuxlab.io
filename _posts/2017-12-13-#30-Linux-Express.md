---
title: "#30 Linux Express"
date: 2017-12-13
author: juan
category: [linuxexpress]
featimg: 2017/30linuxexpress.png
podcast:
  audio: https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/30linuxexpress
  video:
tags: [audio, telegram, Linux Express]
comments: true
---
![](https://podcastlinux.gitlab.io/media/compressed/2017/30linuxexpress.png)
Todos los miércoles, entrega de un podcast, ya sea Linux Express o Podcast Linux. Hoy tienes un Linux Express.

<audio controls>
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/30linuxexpress.ogg" type="audio/ogg">
  <source src="https://gitlab.com/podcastlinux/podcastlinux.gitlab.io/raw/master/Linux-Express/30linuxexpress.mp3" type="audio/mpeg">
</audio>

Último mes del año con muchas cosas en el asador:

+ [Episodio #40 Linux Connexion con Aleix Pol](http://avpodcast.net/podcastlinux/aleixpol).
+ Próximo episodio Gaming y GNU/Linux
+ Se acerca un episodio especial. ¿Qué será?
+ Audios [KillAll Radio Podcast Telegram](https://t.me/killallradiopodcast)
+ [Gpodder](https://gpodder.github.io/) y [Gpodder.net](https://gpodder.net/)

Las imágenes utilizadas son propiedad de [Freepik.es](http://www.freepik.es/)

Recuerda que puedes **contactar** conmigo de las siguientes formas:

Twitter: <https://twitter.com/podcastlinux>  
Correo: <podcastlinux@avpodcast.net>  
Web: <http://avpodcast.net/podcastlinux/>  
Blog: <https://podcastlinux.gitlab.io/>  
Telegram: <https://t.me/podcastlinux>  
Youtube: <https://www.youtube.com/PodcastLinux>  
Feed Podcast Linux: <https://feedpress.me/podcastlinux>  
Feed Linux Express (Audios Telegram): <http://feeds.feedburner.com/linuxexpress>  
